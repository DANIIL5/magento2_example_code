<?php
/**
 * Author: Daniil Bessonov (d.bessonov@kt-team.de)
 * Date: 27.10.2021
 */

namespace SQ2\StretchMark\Model;

use DateTime;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use SQ2\StretchMark\Api\StretchMarkApiInterface;

class GetLimitationsStretchInformation implements StretchMarkApiInterface
{

    /**
     * @param ResourceConnection $resource
     * @param StretchMarkRepository $stretchMarkRepository
     */
    public function __construct(
        ResourceConnection $resource,
        StretchMarkRepository $stretchMarkRepository
    ) {
        $this->resource = $resource;
        $this->stretchMarkRepository = $stretchMarkRepository;
    }

    /**
     * @var ResourceConnection
     */
    protected $resource;
    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    /**
     * @var StretchMarkRepository
     */
    protected $stretchMarkRepository;

    /**
     * {@inheritdoc}
     */
    public function getLimitationsStretchInformation(int $customerId, int $stretchId)
    {
        $connection = $this->resource->getConnection();
        $stretch = $this->stretchMarkRepository->get($stretchId);
        $countDaysAfterOrderCustomer = $stretch->getData('count_days_after_order_customer');
        $excludeIdfmc = $stretch->getData('exclude_idfmc');
        $excludeSubclass = $stretch->getData('exclude_subclass');

        if (!empty($excludeIdfmc) || !empty($excludeSubclass)) {
            $select = $connection->select()
                ->from($connection->getTableName('sales_order'), [])
                ->join(
                    ['sales_order_item'],
                    'sales_order_item.order_id = sales_order.entity_id',
                    []
                )
                ->join(
                    ['catalog_product_entity'],
                    'catalog_product_entity.sku = sales_order_item.sku',
                    ['catalog_product_entity.entity_id as customer_products']
                )
                ->where('customer_id = ?', $customerId)
                ->where('sales_order.status != ?', 'canceled')
                ->where('product_type = ?', 'simple');

            $customerProducts[] = $connection->fetchAll($select);
            if (isset($customerProducts[0]) && !empty($customerProducts[0])) {
                $customerProducts = array_column($customerProducts[0], 'customer_products');
                $customerProducts = implode(',', $customerProducts);
                $subqueryIdfmc = $connection->select()->from($connection->getTableName('eav_attribute'), ['attribute_id'])
                    ->where('entity_type_id = (?)', 4)
                    ->where('attribute_code  = (?)', 'product_idfmc');
                $subquerySubclass = $connection->select()->from($connection->getTableName('eav_attribute'), ['attribute_id'])
                    ->where('entity_type_id = (?)', 4)
                    ->where('attribute_code  = (?)', 'product_idsubclass');
                $orderCustomer = new \Zend_Db_Expr($customerProducts);
                $select1 = $connection->select()
                        ->from($connection->getTableName('catalog_product_entity_varchar'), ['value'])
                        ->where('entity_id in (?)', $orderCustomer)
                        ->where('attribute_id = (?)', $subqueryIdfmc)
                        ->where('store_id = (?)', 0);
                $select2 = $connection->select()
                    ->from($connection->getTableName('catalog_product_entity_int'), ['value'])
                    ->where('entity_id in (?)', $orderCustomer)
                    ->where('attribute_id = (?)', $subquerySubclass)
                    ->where('store_id = (?)', 0);
                $select = $connection->select()
                    ->union([$select1, $select2]);

                $orderCustomerIdfmcSubclass = $connection->fetchAssoc($select);

                if (!empty($orderCustomerIdfmcSubclass)) {
                    $excludeIdfmc = explode(',', $excludeIdfmc);
                    $excludeSubclass = explode(',', $excludeSubclass);
                    $idfmcSubclass = array_merge($excludeIdfmc, $excludeSubclass);
                    foreach ($idfmcSubclass as $value) {
                        if (array_key_exists($value, $orderCustomerIdfmcSubclass)) {
                            return true;
                        }
                    }
                }
            }
        }
        if ($stretch->getData('x_days_after_order_customer')) {
            $select = $connection->select()
                ->from($connection->getTableName('sales_order'), ['sales_order.created_at'])
                ->where('customer_id = ?', $customerId)->limit(1)->order('entity_id DESC');
            $date_customer = $connection->fetchOne($select);

            if (isset($date_customer) && $date_customer != null) {
                $origin = new DateTime($date_customer);
                $target = new DateTime("now");
                $interval = $origin->diff($target);
                if ($interval->days > $countDaysAfterOrderCustomer) {
                    return true;
                }
            }
        }
        return  false;
    }
}
