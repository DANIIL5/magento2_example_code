<?php

declare(strict_types=1);

namespace KT\Telegram\Model\Queue\Message\Processor;

use KT\Telegram\Api\Queue\MessageInterface;
use KT\Telegram\Helper\SendTelegramMessage;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use SQ2\PushNotifications\Model\Campaign\TypeProcessor\Triggered as Triggers;
use SQ2\PushNotifications\Api\CampaignRepositoryInterface;

/**
 * Class Push
 * @package KT\Telegram\Model\Queue\Message\Processor
 */
class Push extends AbstractProcessor
{
    /**
     * @var CampaignRepositoryInterface
     */
    protected $campaignRepository;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var Triggers
     */
    protected $triggered;

    /**
     * Magento constructor.
     *
     * @param SendTelegramMessage $telegramMessage
     * @param Triggers $triggered
     * @param ResourceConnection $resourceConnection
     * @param CampaignRepositoryInterface $campaignRepository
     */
    public function __construct(
        SendTelegramMessage         $telegramMessage,
        Triggers                    $triggered,
        ResourceConnection          $resourceConnection,
        CampaignRepositoryInterface $campaignRepository
    ) {
        parent::__construct($telegramMessage);
        $this->resourceConnection = $resourceConnection;
        $this->triggered = $triggered;
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * @param MessageInterface $message
     * @return bool
     * @throws LocalizedException
     */
    public function process(MessageInterface $message): bool
    {
        try {
            $extendedDataObject = $message->getExtendedDataObject();
            $type = $extendedDataObject->getType();
            switch ($type) {
                case 'get_count_events_by_campaign':
                    $campaignId = $extendedDataObject->getData('campaign_id');

                    if (empty($campaignId)) {
                        $this->telegramMessage->sendDataToQueue([
                            'chat_id' => $message->getChatId(),
                            'text' => __("ID кампании должен быть заполнен"),
                        ]);
                        break;
                    }

                    try {
                        $campaign = $this->campaignRepository->get($campaignId);
                    } catch (NoSuchEntityException $e) {
                        $this->telegramMessage->sendDataToQueue([
                            'chat_id' => $message->getChatId(),
                            'text' => __('Кампания не существует'),
                        ]);
                        break;
                    }

                    $count = $this->triggered->getCountPushNotification($campaign);
                    $this->telegramMessage->sendDataToQueue([
                        'chat_id' => $message->getChatId(),
                        'text' => $count,
                    ]);
            }
        } catch (\Exception $e) {
            $this->telegramMessage->sendDataToQueue([
                'chat_id' => $message->getChatId(),
                'text' => __('Неверный запрос'),
            ]);
        }

        return true;
    }
}
